#include "Read_file.h"

#define FILE_NAME "encrypted.txt"
#define ASCII_LETTER_START 97
#define ALPHABET_LETTER_COUNT 26
#define MOST_USED_LETTER 'e'

int getEncryptionOffset(string file);
string decryptFile(string file, int encOffset);

int main(void)
{
	// Variables
	Read_file* rFile = new Read_file();
	string fileText = rFile->read(FILE_NAME); // reading file's content
	int offset = getEncryptionOffset(fileText);
	string decryptedFileText = decryptFile(fileText, offset);
	cout << decryptedFileText << endl;

	system("pause");
	return 0;
}


///<summary>Finds the encryption offset in the given file's text</summary>
///<param name="file">file's text</param>
int getEncryptionOffset(string file)
{
	// Variables
	int letterCount[ALPHABET_LETTER_COUNT] = { 0 };
	int highestIndex = 0, highestCount = 0;

	// Counting each letter appearance in file
	for (unsigned long int i = 0; i < file.length(); i++)
	{
		if (file[i] >= 'a' && file[i] <= 'z')
		{
			letterCount[file[i] - ASCII_LETTER_START]++;
		}
	}

	// Getting the highest appearing letter in file from the count array
	highestCount = letterCount[0];
	for (int i = 1; i < ALPHABET_LETTER_COUNT; i++)
	{
		if (letterCount[i] > highestCount)
		{
			highestIndex = i;
			highestCount = letterCount[i];
		}
	}

	return highestIndex - (MOST_USED_LETTER - ASCII_LETTER_START); // getting the distance of the most letter from the alphabet most used letter
}


///<summary>returns the file's text decrypted using the encryption offset</summary>
///<param name="file">file's text</param>
///<param name="encOffset">the offset used to encrypt the text</param>
string decryptFile(string file, int encOffset)
{
	int curLetter = 0;
	// making the offset positive
	if (encOffset < 0)
	{
		encOffset += ALPHABET_LETTER_COUNT;
	}
	// Changing each letter in string
	for (unsigned long int i = 0; i < file.length(); i++)
	{
		curLetter = file[i];
		if (curLetter >= 'a' && curLetter <= 'z')
		{
			curLetter -= encOffset;
			// Checking if offset exeeded the letter region
			if (curLetter < 'a')
			{
				curLetter += ALPHABET_LETTER_COUNT;
			}
			file[i] = curLetter;
		}
	}
	return file;
}