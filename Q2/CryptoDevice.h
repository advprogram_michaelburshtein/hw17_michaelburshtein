#pragma once

#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>
#include "des.h"
#include "md5.h"
#include "hex.h"

using namespace std;


class CryptoDevice
{

public:
	string name, password;
	CryptoDevice();
	~CryptoDevice();
	string encryptMD5(string);
	string encryptAES(string);
	string decryptAES(string);
	
};
